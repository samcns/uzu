use v6;

unit module Uzu:ver<0.1.9>:auth<gitlab:samcns>;

# License
# 
# This module is licensed under the same license as Perl6 itself. 
# Artistic License 2.0.
#
# Copyright 2017 Sam Morrison.

# vim: ft=perl6
